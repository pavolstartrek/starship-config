# Starship config

# Configuration
Default path for starship configuration is `~/.config/starship.toml`. It is inconvenient to manage this file by git.

You can change config file location by adding following export before startship init in `.bashrc`.


```bash
export STARSHIP_CONFIG="<path to config>/starship.toml"
eval "$(starship init bash)"
```
